"use strict";

// a list of devices, with their 'id' as key
// it is generally advisable to keep a list of
// paired and active devices in your driver's memory.
var devices = {};
var triggers = {};

// https://www.tutorialspoint.com/nodejs/nodejs_net_module.htm
var net = require("net");

// the `init` method is called when your driver is loaded for the first time
module.exports.init = function (devices_data, callback) {
    console.info("Booting TCP driver");
    devices_data.forEach(function (device_data) {
        initDevice(device_data);
    });

    callback();
};

// the `added` method is called is when pairing is done and a device has been added
module.exports.added = function (device_data, callback) {
    console.log("Adding device " + device_data.id);
    initDevice(device_data);
    callback(null, true);
};

// the `delete` method is called when a device has been deleted by a user
module.exports.deleted = function (device_data, callback) {
    console.log("Deleting device " + device_data.id);
    clearTimeout(triggers[device_data.id]);
    delete devices[device_data.id];
    callback(null, true);
};

// the `pair` method is called when a user start pairing
module.exports.pair = function (socket) {
    console.log("Pairing started");

    socket.on('configure_ip', function (data, callback) {
        console.info("Configuring device");
        callback(null, data);
    })
};

function scanDevice(device, onlineCallback, offlineCallback) {
    var client = new net.Socket();

    var hostTimeout = Homey.manager('settings').get('host_timeout');
    if(!hostTimeout)
        hostTimeout = 2;
    hostTimeout = 1000 * parseInt(hostTimeout);

    var cancelCheck = setTimeout(function() {
        device.state = {ip_present: false};
        client.destroy();
        if (offlineCallback) {
            offlineCallback(device);
        }
    }, hostTimeout);

    client.on('error', function (err) {
        device.state = {ip_present: false};
        console.error(err);
        clearTimeout(cancelCheck);
        if (offlineCallback) {
            offlineCallback(device);
        }
    });


    client.connect(device.settings.tcp_port, device.settings.host, function () {
        device.state = {ip_present: true};
        clearTimeout(cancelCheck);
        if (onlineCallback)
            onlineCallback(device);

        client.destroy()
    });
}


// a helper method to add a device to the devices list
function initDevice(device_data) {
    module.exports.getSettings(device_data, function (err, settings) {
        devices[device_data.id] = {};
        devices[device_data.id].state = {ip_present: false};
        devices[device_data.id].data = device_data;
        devices[device_data.id].settings = settings;

        var reload = function() {
            module.exports.getSettings(device_data, function (err, _settings) {

                var wasOnline = false;
                if(devices[device_data.id] && devices[device_data.id].state &&
                   devices[device_data.id].state.ip_present && _settings) {
                    wasOnline = devices[device_data.id].state.ip_present;
                }

                devices[device_data.id].settings = _settings;
                var state = devices[device_data.id].state;
                var tokens = {"type": "device"};

                var reloadTimeout = Homey.manager('settings').get('host_check_interval');
                if(!reloadTimeout)
                    reloadTimeout = 5;
                reloadTimeout = 1000 * parseInt(reloadTimeout);

                var isOnlineCallback = function (d) {
                    module.exports.setAvailable( device_data );

                    if (!wasOnline) {
                        console.info("Device " + d.settings.host + ":" + d.settings.tcp_port + " came online");
                        Homey.manager('flow').triggerDevice('device_came_online', tokens, state, device_data, function (err, result) {
                                if (err) return console.error(err);
                            }
                        );
                    }
                    triggers[device_data.id] = setTimeout(reload, reloadTimeout);
                };

                var isOfflineCallback = function (d) {
                    module.exports.setUnavailable( device_data, "Offline" );

                    if (wasOnline) {
                        console.info("Device " + d.settings.host + ":" + d.settings.tcp_port + " went offline");
                        Homey.manager('flow').triggerDevice('device_went_offline', tokens, state, device_data, function (err, result) {
                                if (err) return console.error(err);
                            }
                        );
                    }
                    triggers[device_data.id] = setTimeout(reload, reloadTimeout);
                };

                var sanityCheck = function(d) {
                    scanDevice(devices[device_data.id], isOnlineCallback, isOfflineCallback);
                };

                scanDevice(devices[device_data.id], isOnlineCallback, sanityCheck);
            });
        };

        triggers[device_data.id] = setTimeout(reload, 0);
    });
}

// these are the methods that respond to get/set calls from Homey
// for example when a user pressed a button
module.exports.capabilities = {};

module.exports.capabilities.ip_present = {};

module.exports.capabilities.ip_present.get = function (device_data, callback) {
    var device = getDeviceByData( device_data );
    if( device instanceof Error ) return callback( device );
    return callback( null, device.state.ip_present );
};

// a helper method to get a device from the devices list by it's device_data object
function getDeviceByData(device_data) {
    var device = devices[device_data.id];
    if (typeof device === 'undefined') {
        return new Error("Could not find device " + device_data.id);
    } else {
        return device;
    }
}

Homey.manager('flow').on('trigger.device_is_online', function (callback, args) {
    callback(null, true);
});

Homey.manager('flow').on('trigger.device_came_online', function (callback, args) {
    callback(null, true);
});

Homey.manager('flow').on('condition.device_is_online', function (callback, args) {
    var device = getDeviceByData(args.device);

    var isOnlineCallback = function (d) {
        callback(null, true);
    };

    var isOfflineCallback = function (d) {
        callback(null, false);
    };

    var sanityCheck = function(d) {
        scanDevice(device, isOnlineCallback, isOfflineCallback);
    };

    scanDevice(device, isOnlineCallback, sanityCheck);
});

Homey.manager('flow').on('condition.device_is_offline', function (callback, args) {
    var device = getDeviceByData(args.device);

    var isOnlineCallback = function (d) {
        callback(null, false);
    };

    var isOfflineCallback = function (d) {
        callback(null, true);
    };

    var sanityCheck = function(d) {
        scanDevice(device, isOnlineCallback, sanityCheck);
    };

    scanDevice(device, isOnlineCallback, isOfflineCallback);
});
